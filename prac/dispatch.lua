-- dispatch.lua

module(...,package.seeall)

local function listener()

	local self = {}
	self.listenerTable = {}

	-- データ初期化
	function self:initialize()

		self.name = nil
		self.year = nil
		self.comment = nil

	end

	-- リスナー追加
	function self:addEventListener(listener)

		assert(listener,"Error not found !")

		local allow = true

		for k,v in pairs(self.listenerTable) do

			if listener == v then

				local allow = false

			end

		end

		if allow == true then

			table.insert(self.listenerTable,listener)
			return 1
		else
			return -1

		end

	end

	-- リスナー除去
	function self.removeEventListener(liseter)

		for k,v in pairs(self.listenerTable) do

			if listener == v then

				table.remove(self.listenerTable,k)

			end
		end
	end

	-- リスナーにイベントを渡す
	function self:dispatchEvent(event)

		for k,v in pairs(self.listenerTable) do

			v(event)

		end

	end

	function self:setData(name,year,comment)

		self.name = name
		self.year = year
		self.comment = comment

	end

	self:initialize()


	return self


end

function new()

	return listener()

end

