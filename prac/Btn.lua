-- Btn.lua

module(...,package.seeall)

local composer = require("composer")

local function listener()

	local self = {}

	function self.composerBtn(X,Y,word,file,option)

		local group = display.newGroup()
		local btn = display.newRect(group,300,300,X,Y)
		btn:setFillColor(0.3,0.2,0.4)
		local text = display.newText(group,word,btn.x,btn.y,nil,40)
		text:setFillColor(0.3,0.5,0.9)

		local function composerListener(event)

			if event.phase == "began" then 

				btn:setFillColor(0.3,0.4,0.5)

			elseif event.phase == "ended" then

				btn:setFillColor(0.3,0.2,0.4)

				composer.gotoScene(file,option)

			end

			return true
		end

		btn:addEventListener("touch",composerListener)

		return group
	end

	return self

end

function new()

	return listener()

end
