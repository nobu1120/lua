-- question2.lua

-- 【問題２】
-- 動いているtimerをまとめて止める関数を作成して下さい

-- 【問題2】 Answer
local function allStop( timerTable )
	for k,v in pairs( timerTable ) do

		timer.cancel(v)
		k = nil ; v = nil
	end
end

local function listener1()
	print("1")
end
local function listener2()
	print("2")
end
local function listener3()
	print("3")
end
local function listener4()
	print("4")
end
local function listener5()
	print("5")
end

local timer1 = timer.performWithDelay( 100 , listener1 ,0)
local timer2 = timer.performWithDelay( 200 , listener2 ,0)
local timer3 = timer.performWithDelay( 300 , listener3 ,0)
local timer4 = timer.performWithDelay( 400 , listener4 ,0)
local timer5 = timer.performWithDelay( 500 , listener5 ,0)

local timerTable = {}

timerTable[#timerTable+1] = timer1
timerTable[#timerTable+1] = timer2
timerTable[#timerTable+1] = timer3
timerTable[#timerTable+1] = timer4
timerTable[#timerTable+1] = timer5


local function stopTimer(event)
	allStop( timerTable )
end

local btn = display.newRect(200,200,200,200)
btn:addEventListener("tap",stopTimer)

-- timerTableの中身表示
local function displayTable()
	for k,v in pairs(timerTable) do
		for k,v in pairs(v) do
			print(k)
			print(v)
		end
	end
end

local btn1 = display.newRect(200,400,100,100)
btn1:addEventListener("tap",displayTable)
