-- question1.lua

-- 【問題１】
-- レスポンスが返ってくる前にリクエストが再度かれた時、最も最新のリクエストのレスポンスのみを拾うには?

local json = require("json")

-- コメントを表示するセルを作成
local function createCell(data)

	local group = display.newGroup()

	-- セル関連
	local cell = display.newRect(group,display.contentWidth/2 + 2.5 ,90,display.contentWidth-10,200)
	cell:setFillColor(0.2,0.3,0.4)
	local cellTopFlame = display.newRect(group,cell.x,cell.y - 95,display.contentWidth,5)
	local cellLeftFlame = display.newRect(group,2.5,cell.y,5,200)
	local cellRightFlame = display.newRect(group,display.contentWidth - 5,cell.y,5,200)
	local cellBottomFlame = display.newRect(group,cell.x,cell.y + 95,display.contentWidth,5)
	
	-- コメント関連
	local tweet = display.newText(group,"tweet:"..data.tweet,0,0,nil,40)
	tweet:setFillColor(0.6,0.7,0.8)
	tweet.x , tweet.y = cell.x , cell.y

	local date = display.newText(group,"日付"..data.date,0,0,nil,15)
	date:setFillColor(0.6,0.7,0.8)
	date.x , date.y = cell.x - 200 , cell.y - 50 

	return group

end


local function request()

	local function listener(event)
	    if event.isError == true then
	        print( "Network error!" )
	    else
	        print( "GET: " .. event.response )

	        if event.response then
				local data = json.decode(event.response)
				if data then
					for k,v in pairs(data) do
						local cell = createCell(v)
						cell.y = 200*k
					end
				end
			end
		end
	end
	
	network.request( "http://localhost:8888/getTweet.php" , "GET" , listener )
end

local newResponse = {}

-- 最新のレスポンスだけ受け取る
local function newRequest()

	local function listener(event)

	    if event.isError == true then
	        print( "Network error!" )
	    else
	        print( "GET: " .. event.response )

	        if event.response then
				local data = json.decode(event.response)
				if data then
					for k,v in pairs(data) do
						if k == #data then
							newResponse = v
						end
					end
				end
			end
		end
	end

	network.request( "http://localhost:8888/getTweet.php" , "GET" , listener )
end

local function post( text )

	-- The following code demonstrates sending data via HTTP POST,
	-- specifying custom request headers and request body.

	local function networkListener( event )

	    if ( event.isError ) then
	        print( "Network error!" )
	    else
	        print ( "POST: " .. event.response )
	        request()
	    end
	end

	local headers = {}

	headers["Content-Type"] = "application/x-www-form-urlencoded"
	headers["Accept-Language"] = "ja,en-us;q=0.7,en;q=0.3"

	local body = "text="..text

	local params = {}
	params.headers = headers
	params.body = body

	network.request( "http://localhost:8888/postTweet.php", "POST", networkListener, params )

end

local function createBtn( text , X , Y , action )

	local group = display.newGroup()
	local btn = display.newRect(group,X,Y,200,200)
	local btnText = display.newText(text,0,0,nil,40)
	btnText:setFillColor(0.2,0.5,0.7)
	btnText.x , btnText.y = btn.x , btn.y

	local function touchListener(event)
		local phase = event.phase

		if phase == "began" then
			event.target:setFillColor(0.4,0.5,0,5)
			post( text )
		elseif phase == "ended" then
			event.target:setFillColor(1)
		end
	end

	btn:addEventListener("touch",touchListener)

	return group
end

local postBtn = createBtn("www",300,200)

local deleteBtn = createBtn("delete",300,600)

local getNewDataBtn = display.newRect(500,800,100,100)

local function touchListener( event )

	if event.phase == "began" then
		event.target:setFillColor(0.4,0.5,0,5)
	elseif event.phase == "ended" then
		event.target:setFillColor(1)
		newRequest()
		print("------------------------")
		print(newResponse.tweet)
		print(newResponse.date)
		print("------------------------")

	end
end

getNewDataBtn:addEventListener("touch",touchListener)


-- 一応php側
-- postTweet.php
--[[
<?php 
	
	// main.luaから受け取ったtweetをセッションとして配列に保持
	session_start();

	if (isset($_POST["text"]) && $_POST["text"] == "delete"){
		unset($_SESSION["tweet"]);
	}

	// 一回目
	if (isset($_POST["text"]) && empty($_SESSION["tweet"]) && $_POST["text"] != "delete" ){

		$tweet = array( "tweet" => $_POST["text"] , "date" => date("H:i:s") );
		$_SESSION["tweet"] = array( $tweet );
		$tweet_encode = json_encode( $_SESSION["tweet"]);
		echo $tweet_encode;

	}elseif( isset( $_POST["text"]) && !empty( $_SESSION["tweet"] ) ){

		$tweet = array( "tweet" => $_POST["text"] , "date" => date("H:i:s") );
		$tweet_encode = json_encode( $tweet );
		echo $tweet_encode;
		array_push( $_SESSION["tweet"] , $tweet );

		// 履歴削除
		// unset($_SESSION["tweet"]);

	}else{
		echo "Error : not found data !";
	}

?>

]]


-- getTweet.php

--[[

<?php
	
	session_start();
	
	// tweet を main.lua へ返す(json_encode形式)
	$all_tweet = json_encode( $_SESSION["tweet"] );
	echo $all_tweet; 

?>

]]
