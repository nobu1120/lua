-- question5.lua

-- 【問題5】
-- 数字を画像で表示する関数
-- 1.png, 2.png ~ 0.pngまでの画像があるときに 特定の数字を入れると画像の数字で表示する関数

-- 【問題5】 Answer

function showNumber(num)
    if type(num) == "number" then
        local group = display.newGroup()
        numChar = tostring(num)
        l = string.len(numChar)
        local str = {}
        for i = 1 , l do
            str[i] = string.sub(numChar , i , i )
            display.newImage(group,str[i]..".png",100+100*i,100)
            print(str[i])
        end
    else
        print("Bad argument ! Not number")
    end
    return group
end
local s = 1230
local obj = showNumber(s)